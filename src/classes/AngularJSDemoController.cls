global class AngularJSDemoController{ 
    
    public String AccountList { get; set; }
    
    //Subclass : Wrapper Class 
    public class Accountwrap {
        //Static Variables 
        public string id;
        public string name;
        public string Phone;
        public string Fax;
        public string Website;
        
        //Wrapper  Class Controller
        Accountwrap() {
            Phone = '';
            Fax = '';
            Website = '';
        }
        
    }
       
    //Method to bring the list of Account and Serialize Wrapper Object as JSON
    public  static String getlstAccount() {
        List < Accountwrap > lstwrap = new List < Accountwrap > ();
        List < account > lstacc = [SELECT Id, Name, Phone,Fax,Website
                                   FROM Account order by name,createddate limit 20 
                                  ];
        for (Account a: lstacc) {
            Accountwrap awrap = new Accountwrap();
            awrap.id = a.id;
            awrap.name = a.name;
            if (a.Phone != null) {
                awrap.Phone = a.Phone;
            }
            if (a.Fax != null) {
                awrap.Fax = a.Fax;
            }
            if (a.Website != null) {
                awrap.Website = a.Website;
            }
            lstwrap.add(awrap);
        }
        return JSON.serialize(lstwrap);
     }
     
     @RemoteAction 
     public static void deleteAccount(string id){
     
         for(Account acc : [select id from account where id=:id])
             delete acc;

     }
     
}