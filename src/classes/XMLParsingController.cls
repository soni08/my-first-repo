public class XMLParsingController{

    public static void parseXMLBusinussResponse(){
        
        string response = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><SearchByABNv201408Response xmlns="http://abr.business.gov.au/ABRXMLSearch/"><ABRPayloadSearchResults><request><identifierSearchRequest><authenticationGUID>af48b1ae-47ec-4f76-a849-85198385e592</authenticationGUID><identifierType>ABN</identifierType><identifierValue>48167018304</identifierValue><history>Y</history></identifierSearchRequest></request><response><usageStatement>The Registrar of the ABR monitors the quality of the information available on this website and updates the information regularly. However, neither the Registrar of the ABR nor the Commonwealth guarantee that the information available through this service (including search results) is accurate, up to date, complete or accept any liability arising from the use of or reliance upon this site.</usageStatement><dateRegisterLastUpdated>2017-03-10</dateRegisterLastUpdated><dateTimeRetrieved>2017-03-10T20:59:33.0330185+11:00</dateTimeRetrieved><businessEntity201408><recordLastUpdatedDate>2015-09-11</recordLastUpdatedDate><ABN><identifierValue>48167018304</identifierValue><isCurrentIndicator>Y</isCurrentIndicator><replacedFrom>0001-01-01</replacedFrom></ABN><entityStatus><entityStatusCode>Active</entityStatusCode><effectiveFrom>2013-11-29</effectiveFrom><effectiveTo>0001-01-01</effectiveTo></entityStatus><ASICNumber>167018304</ASICNumber><entityType><entityTypeCode>PRV</entityTypeCode><entityDescription>Australian Private Company</entityDescription></entityType><goodsAndServicesTax><effectiveFrom>2013-11-29</effectiveFrom><effectiveTo>0001-01-01</effectiveTo></goodsAndServicesTax><mainName><organisationName>CHAMP (SA) PTY LTD</organisationName><effectiveFrom>2013-11-29</effectiveFrom></mainName><mainBusinessPhysicalAddress><stateCode>SA</stateCode><postcode>5066</postcode><effectiveFrom>2014-09-25</effectiveFrom><effectiveTo>0001-01-01</effectiveTo></mainBusinessPhysicalAddress><mainBusinessPhysicalAddress><stateCode>SA</stateCode><postcode>5066</postcode><effectiveFrom>2014-04-12</effectiveFrom><effectiveTo>2014-09-25</effectiveTo></mainBusinessPhysicalAddress><mainBusinessPhysicalAddress><stateCode>SA</stateCode><postcode>5066</postcode><effectiveFrom>2014-01-31</effectiveFrom><effectiveTo>2014-04-12</effectiveTo></mainBusinessPhysicalAddress><mainBusinessPhysicalAddress><stateCode>SA</stateCode><postcode>5066</postcode><effectiveFrom>2013-11-29</effectiveFrom><effectiveTo>2014-01-31</effectiveTo></mainBusinessPhysicalAddress><businessName><organisationName>Tasmanian Dairy Company</organisationName><effectiveFrom>2015-04-16</effectiveFrom></businessName><businessName><organisationName>The Cheese Partners</organisationName><effectiveFrom>2014-05-07</effectiveFrom></businessName><businessName><organisationName>Inspire Solutions</organisationName><effectiveFrom>2015-04-02</effectiveFrom><effectiveTo>2015-09-05</effectiveTo></businessName></businessEntity201408></response></ABRPayloadSearchResults></SearchByABNv201408Response></soap:Body></soap:Envelope>';
        XMLDom1 dom = new XMLDom1 (response);
        List<mainWrapper> wrapList = new List<mainWrapper>();
        Map<String,mainWrapper> businessMap = New Map<String,mainWrapper>();
     
        
        if(response != null){
          List<XMLDom1.Element> element_list = dom.getElementsByTagName('businessEntity201408');
          for(XMLDom1.Element element:element_list){
                
                  for(XMLDom1.Element child1:element.childNodes){
                      mainWrapper wrapObj = New mainWrapper();
                      system.debug('node ---->'+ child1.nodename);
                      for(XMLDom1.Element child2:child1.childNodes){
                          //if parent is ABN tag then get identifier value
                          if(child1.nodename.toLowerCase() =='abn'){
                              if(child2.nodeName.toLowerCase()=='identifiervalue'){
                                  wrapObj.ABNIdentifier = child2.nodeValue;
                              }
                          } 
                          if(child1.nodename.tolowercase() == 'businessname'){   
                              if(child2.nodeName.toLowerCase()=='organisationname'){
                                  wrapObj.BusinessMainName = child2.nodeValue;
                              }
                          }
                      }
                      wrapList.add(wrapObj);
                  }
           }
        }          
           system.debug('----->'+wrapList) ;
      /*
            
            while(reader.hasNext()) {
                // Manage the abn section for xml
                
                // ABN
                if ('ABN' == reader.getLocalName()) {
                    ISABN = !ISABN ;
                    
                }
                if ('identifierValue' == reader.getLocalName() && ISABN) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && String.IsNotBlank(reader.getText()))
                        wrapObj.ABNIdentifier = reader.getText();
                    }
                }
                
                // TYPE
                if ('entityStatusCode' == reader.getLocalName()) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                        wrapObj.BusinessStatusCode = reader.getText();
                    }
                }
                
                //
                if ('ASICNumber' == reader.getLocalName()) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                        wrapObj.BusinessASICNumber = reader.getText();
                    }
                }
                
                // Type Code
                if ('entityTypeCode' == reader.getLocalName() ) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                        wrapObj.BusinessTypeCode = reader.getText();
                    }
                }
                
                // State Code
                if ('stateCode' == reader.getLocalName()) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                        wrapObj.MainBusinessPhysicalStateCode = reader.getText();
                    }
                }
                
                // Orgnization 
                if ('businessName' == reader.getLocalName()) {
                    IsOrg= true;
                }
                if ('organisationName' == reader.getLocalName() && IsOrg) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(String.isNotBlank(reader.getText())){
                            BusinussName = reader.getText();
                            businessMap.put(reader.getText() ,'');
                        }
                    }
                    
                    else if('effectiveFrom' == reader.getLocalName() && IsOrg){
                        reader.next();
                        if(reader.getEventType() == XmlTag.CHARACTERS){
                            if(businessMap.containsKey(BusinussName )){
                                if(reader.getText() != null && string.IsNotBlank(reader.getText())){
                                    businessMap.put(BusinussName,reader.getText());
                                    IsOrg= !IsOrg;
                                }
                            }
                        }
                    }
                }
                
                // Main Name 
                if('mainName' == reader.getLocalName() ){
                    isMainName = true ;
                }
                if('organisationName' == reader.getLocalName() && isMainName){
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                        wrapObj.entityName = reader.getText();
                    }
                }
                if ('effectiveFrom' == reader.getLocalName() && isMainName) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText())){
                            wrapObj.entityEffDate = ' '+reader.getText();
                            isMainName = !isMainName ;
                        }
                    }
                }
                // goodsAndServicesTax
                if('goodsAndServicesTax' == reader.getLocalName()){
                    isGst = true;
                   system.debug('its enetered gst------>') ;
                }
                system.debug('gst value---->' + isGst);
                if ('effectiveFrom' == reader.getLocalName() && isGst ) {
                    reader.next();
                    system.debug('it has entered------> **');
                    if(reader.getEventType() == XmlTag.CHARACTERS){    
                        system.debug('its deep in ------>');
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                            wrapObj.entityGST = reader.getText();
                        system.debug('its entered in inner section------->');
                    }
                }
                
                // entityType
                if('entityType' == reader.getLocalName()){
                    reader.next();
                    if('entityTypeCode' == reader.getLocalName()){
                        reader.next();
                        if(reader.getEventType() == XmlTag.CHARACTERS){  
                            if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                            wrapObj.BusinessTypeCode = reader.getText();
                        }
                    }
                }
                // legalName
                if('legalName' == reader.getLocalName() ){
                    islegal = !islegal ;
                }       
                if ('givenName' == reader.getLocalName() && islegal ) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                        wrapObj.entityName= ' '+reader.getText();
                    }
                }
                if ('otherGivenName' == reader.getLocalName() && islegal ) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        wrapObj.entityName+= ' '+reader.getText();
                    }
                }
                if ('familyName' == reader.getLocalName() && islegal) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                        wrapObj.entityName+= ' '+reader.getText();
                    }
                }
                if ('effectiveFrom' == reader.getLocalName() && islegal) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                        wrapObj.entityEffDate = ' '+reader.getText();
                    }
                }
                
                // POSTCODE
                if ('postcode' == reader.getLocalName()) {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        if(reader.getText() != null && string.IsNotBlank(reader.getText()))
                        wrapObj.MainBusinessPhysicalPostCode = reader.getText();
                    }
                }
                
                 reader.next();
            }
            
          
                    mainWrapper wrapObjTemp = New mainWrapper();
                    wrapObjTemp.score= score;
                    wrapObjTemp.BusinessMainName = name;
                    wrapObjTemp.ABNEffectiveFrom = businessMap.get(name);
                    wrapObjTemp.score= score;
                    wrapObjTemp.MainBusinessPhysicalPostCode = wrapObj.MainBusinessPhysicalPostCode ;
                    wrapObjTemp.MainBusinessPhysicalStateCode  = wrapObj.MainBusinessPhysicalStateCode ;
                    wrapObjTemp.BusinessTypeCode = wrapObj.BusinessTypeCode ;
                    wrapObjTemp.BusinessASICNumber  = wrapObj.BusinessASICNumber ;
                    wrapObjTemp.BusinessStatusCode  = wrapObj.BusinessStatusCode ;
                    wrapObjTemp.ABNIdentifier  = wrapObj.ABNIdentifier ;
                    wrapObjTemp.entityName = wrapObj.entityName ;
                    wrapObjTemp.entityGST = wrapObj.entityGST ;
                    wrapList.add(wrapObjTemp);
                }
         */  
    }
     //  Wrapper classes
    public class mainWrapper{
       public String ABNIdentifier{get;set;}
       public String ABNEffectiveFrom{get;set;}
       public String BusinessMainName{get;set;}
       /*public String BusinessStatusCode{get;set;}
       public String BusinessASICNumber{get;set;}
       public String BusinessTypeCode{get;set;}
       public String MainBusinessPhysicalStateCode{get;set;}
       public String MainBusinessPhysicalPostCode{get;set;}
       public String score{get;set;}
       public String entityName{get;set;}
       public String entityGST{get;set;}
       public String entityEffDate{get;set;} */
       
    }
}