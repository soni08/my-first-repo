public class HackURL{

    String title;
    String accountid;
    public List<Account> accList{get;set;}
     String accountname;
    String prefix;
    public  HackURL(ApexPages.StandardController controller){
    
        String id = apexpages.currentpage().getparameters().get('id');
        if(id != null){
          prefix = id.substring(0,3);
          title= [select id, accountid,title from contact where id=:id].title;
          accountid= [select id, accountid,title from contact where id=:id].accountid;
          accountname= [select id,account.name, accountid,title from contact where id=:id].account.name;
        }
    
    }
    public HackURL(){
    
        accList = new List<Account>();
        accList  = [select id,name from account];
    }
    
    public pagereference redirect(){
    
        return new pagereference ('https://ap2.salesforce.com/'+prefix+'/e?con5='+title+'&con4='+accountname+'&con4_lkid='+accountid);
    
    
    }



}