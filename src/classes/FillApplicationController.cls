public class FillApplicationController{

    public string jobId{get;set;}
    public application__c app{get;set;}
    public string username{get;set;}
    public attachment attach{get;set;}
    public List<User> userList{get;set;}
    public string post{get;set;}
    public string TestingSubmitFunction{get;set;}
    public boolean isApplied{get;set;}
    List<application__C> applicationList;
    public List<string> skills{get;set;}
    public List<SelectOption> requiredSkillsList{get;set;}
    List<Job__C > jobList{get;set;}
    public FillApplicationController(){
    
        skills = new List<string>();
        requiredSkillsList = new List<SelectOption>();
        app = new Application__C();
        jobList = new   List<Job__C >();
        userList = new List<User>();
        applicationList = new  List<application__C>();
        jobId = apexpages.currentpage().getParameters().get('id');
        jobList = [select name,skills__C from job__C where id=:jobId limit 1];
        if(jobList !=null && jobList.size()>0)
            post =  jobList[0].name;
        attach = new attachment();
        userList  = [select contact.name,contactId,contact.email from user where id=:userinfo.getUserid() and contactid !=null];
        if(userList  !=null && userList.size()>0){
            username = userList[0].contact.name;  
            applicationList = [select id from application__C where job__c=:jobId and applicant__c=:userList[0].contactid];
            
            if( applicationList !=null && applicationList.size()>0)  
                isApplied =true;   
        }  
      
       if(jobList !=null && jobList.size()>0 && jobList[0].skills__C !=null){
            for(String str : jobList[0].skills__C.split(','))
                requiredSkillsList.add(new selectOption(str,str)); 
        } 
    }
    
    public void submit(){
  
        system.debug('------------->'+ skills);
        app.job__C =  jobId;
        app.applicant__C = userList[0].contactid;
        string temp ='';
        for(string str : skills){
            temp += str +',' ;
        }
        app.skills__c =temp;
        insert app;
        
        attach.name = username +'_'+post;
        attach.parentid = app.id ;
        try{
            insert attach;
            isApplied =true;  
            
            if(UserList[0].contact !=null && userList[0].contact.email !=null)
                sendEmail(userList[0].contact.email, userList[0].contactId,attach.id);          
            
            
            
       }catch(Exception e){system.debug('@@@'+e.getMessage());}    
    }

     public pagereference logOut(){
     
        return new pagereference('/secur/logout.jsp');
     }
     
     public void sendEmail(String maidId,string contId,string attachmentId){
     
       List<Messaging.SingleEmailMessage> mailList = new List <Messaging.SingleEmailMessage>();
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setToAddresses(new String[] { maidId});
 
        mail.setSubject('Resume Submission');
        mail.setPlaintextbody('Dear User, \n \t Your Resume has been submitted. \n\n\n Thank You');
        mailList.add(mail);    
        /*List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        for (Attachment a : [select Name, Body, BodyLength from Attachment where Id = :attachmentId]) {  // Add to attachment file list  
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();  
            efa.setFileName(a.Name);
            efa.setBody(a.Body);
            fileAttachments.add(efa);
        }

        mail.setFileAttachments(fileAttachments);*/
        Messaging.sendEmail(mailList);
 
    }
          
     


}