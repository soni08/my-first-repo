public  class CheckPermissionController{

    public boolean ispermission{get;set;}
    public CheckPermissionController(){
        ispermission = true;
            SObjectType obj = Schema.getGlobalDescribe().get('job__c');
            if(obj != null){
                DescribeSObjectResult objDef = obj.getDescribe();
                if(objDef.isDeletable() == false){
                    ispermission = false;
               }
            }   
    }            
}