/**

    Class Name    :    CalendarController
    Date          :    Jan 8, 2016
    Developer     :    Sagar soni (Ibirds)
    Description   :    This controller is used to generate dynamic calendar.

*/


public class CalendarController{ 

    public boolean isError{get;set;}
    public string divId{get;set;}
    public string monthName{get;set;}
    public String month{get;set;} 
    public string year{get;set;}
    public List<ModalCal> modalList{get;set;}
    public integer yr{get;set;}
    public string getDateData(){
        Date dt;
        integer i;
        modalList = new List<ModalCal>();
        try{
            dt  = Date.newInstance(Integer.valueOf(year),Integer.valueOf(month),1);
        }catch(exception e){ isError =true;}
        if(dt !=null){
            divId = month+yr;
        yr =dt.year();
            ModalCal modal = new ModalCal();
            List<String> dayList = new List<String>();
            set<integer> holidays = new set<Integer>{4,5,9,23};
            modal.days = new List<ModalDays>();
            Time tm = Time.newInstance(0,0,0,0);
            Integer totalDays = Date.daysInMonth(dt.year(),dt.month());
            monthName = DateTime.newInstance(dt,tm).format('MMMM');
            for(i=1;i<=totalDays;i++){
                if(i==1){
                //checking for starting position of date 1 in calendar
                   String wk = DateTime.newInstance(dt,tm).format('EEE'); 
                   if(wk=='Tue')
                         modal.days = getDays(1); 
                   else if(wk=='Wed')
                         modal.days= getDays(2);  
                   else if(wk=='Thu')
                         modal.days = getDays(3); 
                   else if(wk=='Fri')
                         modal.days = getDays(4);      
                   else if(wk=='Sat')
                         modal.days=getDays(5);  
                   else if(wk=='Sun')
                         modal.days=getDays(6);                    
                }        
                ModalDays md = new ModalDays();
                md.day =String.valueOf(i);
               if(DateTime.newInstance(dt,tm).format('EEE')=='Sun' || holidays.contains(dt.day()))
                     md.isHoliday=true; 
                modal.days.add(md); 
                 
                if(modal.days.size()==7){                        
                    modalList.add(modal);
                    modal = new ModalCal();
                    modal.days = new List<ModalDays>();  
                }else{
                    if(i==totalDays)
                          modalList.add(modal);      
                }             
                dt = dt.adddays(1);  
            }
        }    
        return '';
    }
    //This method is used to adjust starting point of Calendar      
    private List<ModalDays> getDays(Integer i){   
        List<ModalDays> strList = new List<ModalDays>();
       
        for(integer j=0;j<i;j++){
           ModalDays md = new ModalDays();
           md.day='';
           strList.add(md);
        }                
        return strList;    
    }
    public class ModalCal{   
        public list<ModalDays> days{get;set;}    
    }
    
    public class ModalDays{
        public string day{get;set;}
        public boolean isHoliday{get;set;} 
    }   
}