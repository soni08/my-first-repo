public class InstaConnectController{
    String code;
    Attachment attachment;
    private string redirect_uri = 'https://c.ap2.visual.force.com/apex/Insta_Connect' ;  
    private string key = 'ee8ee488c6c64af9b7235d8ef3a4bcbe';  
    private string secret = 'cabd8d8ec7ad408e90e25033609e2b6f' ;  
    public List<Example> tokenInfo{get;set;}
    public  String responseString{get; set;}
    public string token{get;set;}
    public InstaConnectController(){
        tokenInfo = new List<Example>();
        code = ApexPages.currentPage().getParameters().get('code') ;
        if(code !=null){
            getAccessToken();
        }
    }
    
    public PageReference login() { 
        PageReference pg = new PageReference('https://api.instagram.com/oauth/authorize/?client_id=ee8ee488c6c64af9b7235d8ef3a4bcbe&redirect_uri=https://c.ap2.visual.force.com/apex/Insta_Connect&response_type=code') ;
        return pg ; 
    } 

    public void getAccessToken(){
      
            //Getting access token from google  
            HttpRequest req = new HttpRequest();  
            req.setMethod('POST');  
            req.setEndpoint('https://api.instagram.com/oauth/access_token');  
            req.setHeader('content-type', 'application/x-www-form-urlencoded');  
            String messageBody = 'code='+code+'&client_id='+key+'&client_secret='+secret+'&redirect_uri='+redirect_uri+'&grant_type=authorization_code';  
            req.setHeader('Content-length', String.valueOf(messageBody.length()));  
            req.setBody(messageBody);  
            req.setTimeout(60*1000);  
      
            Http h = new Http();  
            String resp;  
            HttpResponse res = h.send(req);  
            resp = res.getBody();  
            if(resp !=null){
               resp = '[' + resp + ']';
               tokenInfo = (List<Example>)JSON.deserialize(resp,List<Example>.class);
            }
            if(tokenInfo.size()>0){
               token  = tokenInfo[0].access_token ;
              
            }
        
    }
    public void postText(){
       /* String endPoint = 'https://api.instagram.com/v1/users/self?access_token=' + token;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPoint);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        system.debug('------- hit' +response );
        responseString = response.getBody();
       
           */      
        
        String endPoint = 'https://api.instagram.com/v1/media/upload?access_token=' + token;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        //  request.setHeader('Content-Type', 'text/pain');
       // String body = '{\"data\": [{ \"created_time\": \"1280780324\",\"text\": \"Really amazing photo!\",\"from\": {\"username\": \"snoopdogg\", } },]}';  
       request.setBody('{"data": {"type": "image", "users_in_photo": [], "filter": "Walden", "tags": [],"comments": { "count": null }, "caption": null, "likes": { "count": null }, "link": null,"user": {"username" : "yuvraj.lohiya"}, "created_time": null, "images": {"low_resolution": {"url": "http://distillery.s3.amazonaws.com/media/2010/07/16/4de37e03aa4b4372843a7eb33fa41cad_6.jpg", "width": 306,"height": 306 }},"id": "3", "location": null}}');
       // request.setBody('this is my post');
        system.debug('--------going to hit');
        HttpResponse response = http.send(request);
          system.debug('------- hit' +response );
        responseString = response.getBody();
        if(response.getStatusCode() == 200){
            
     
        }
        
         
        //return responseString;
    }
     public class User
    {
        public string username { get; set; }
        public string bio { get; set; }
        public string website { get; set; }
        public string profile_picture { get; set; }
        public string full_name { get; set; }
        public string id { get; set; }
    }

    public class Example
    {
        public string access_token { get; set; }
        public User user { get; set; }
    }
  
}