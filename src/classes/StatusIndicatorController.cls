public class StatusIndicatorController{

    public string status{get;set;}

    public StatusIndicatorController(apexpages.standardController std){
    
       application__C app = ((Application__C)std.getRecord());
       status = [select status__c from application__c where id=:app.id].status__c;
    }

}