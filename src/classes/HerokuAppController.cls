public class HerokuAppController{


    public string result{get;set;}
    public List<ContactModel> contactList {get;set;}
    public HerokuAppController(){
    
        HttpRequest rqst=new HttpRequest();
        rqst.setEndPoint('https://heroku-postgres-20465fa1.herokuapp.com/');
        rqst.setMethod('GET');
        
        Http obj=new Http();
        HttpResponse res= obj.send(rqst);
        result =  res.getBody();
         contactList = new List<ContactModel>();
        if(result !=null){
        
            contactList  = (List<ContactModel>)JSON.deserialize(result,List<ContactModel>.class);
                 
        }         
    }
    
    public class ContactModel{
    
        public String firstname{get;set;}
        public String id{get;set;}
        public String lastname{get;set;}
        public boolean wannaInsert{get;set;}
    }   
}