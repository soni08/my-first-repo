public class CallOut{
    public string result{get;set;}
    public List<Response> contList{get;set;}
    public CallOut(){
        contList = new List<Response>();
    }
    
    public void call(){
    
        String sessionId =userInfo.getSessionId();
        system.debug('!!!!'+sessionId);
        Http htp = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
        req.setHeader('Content-Length','2000');
        req.setEndPoint('https://ap2.salesforce.com/services/apexrest/MyRest');
        req.setHeader('Authorization','OAuth '+sessionId);
        req.setBody('sagar');
        HttpResponse res = htp.send(req);
        //{name:sagar soni}
        system.debug('####'+res.getBody());
        result=res.getBody();
        contList = new List<Response>();
        if(result!=null)
            contList = (List<Response>)JSON.deserialize(result,List<Response>.class);
    }
    public void sendEmail(){
    
        String sessionId =userInfo.getSessionId();
        Http htp = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('https://ap2.salesforce.com/services/apexrest/MyRest/00328000006hh0w');
        req.setHeader('Authorization','OAuth '+sessionId);
        HttpResponse rsp = htp.send(req);
        result=rsp.getBody();
    }
      public void  getJsonResults() {
        String address1,address2;
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        
        address1='Ajmer,+India';
        address2= 'Jaipur,+India';
        req.setMethod('GET');
        
        String url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
            + '?origins=' + address1
            + '&destinations=' + address2
            + '&mode=driving'
            + '&sensor=false'
            + '&language=en'
            + '&units=imperial';
            
        req.setEndPoint(url);
        HTTPResponse resp = http.send(req);    
        result=resp.getBody();
        //return result;
    }
    public class Response{
        public String firstname{get;set;}
        public String lastname{get;set;}
    }

}