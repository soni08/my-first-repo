public class MetaDataApiExampleController{

   public String objName{get;set;}
   public String msg{get;set;}
   public String selectedObj{get;set;}
   public List<SelectOption> options{get;set;}
   public String fieldName{get;set;}
    
   public MetaDataApiExampleController(){
      
       options  = getAllObjects();
   } 
   public void createObject(){
        if(String.isNotBlank(objName)){
            MetadataService.MetadataPort service = createService();
            MetadataService.CustomObject customObject = new MetadataService.CustomObject();
            customObject.fullName = objName.trim() + '__c';
            customObject.label = objName.trim();
            customObject.pluralLabel = objName.trim() + 's';
            customObject.nameField = new MetadataService.CustomField();
            customObject.nameField.type_x = 'Text';
            customObject.nameField.label = 'Test Record';
            customObject.deploymentStatus = 'Deployed';
            customObject.sharingModel = 'ReadWrite';
            List<MetadataService.SaveResult> results =
                service.createMetadata(
                    new MetadataService.Metadata[] { customObject });
         
            system.debug('result------>' + results);
            if(results.size()>0){
                if(results[0].success){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Object created successfully.');
                    ApexPages.addMessage(myMsg);
           
                }else{
                    if(results[0].errors.size()>0){
                        msg = results[0].errors[0].message;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,msg );
                        ApexPages.addMessage(myMsg);
                    }
                }
            }
        }else{
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Object name is required.');
              ApexPages.addMessage(myMsg);
            
        } 
        options  = getAllObjects();
    }
    
    public void createField(){
        if(String.isNotBlank(fieldName)){
            MetadataService.MetadataPort service = createService();
            MetadataService.CustomField customField = new MetadataService.CustomField();
            customField.fullName = selectedObj.trim()+'.'+fieldName.trim()+'__c';
            customField.label = fieldName.trim();
            customField.type_x = 'Text';
            customField.length = 42;
            List<MetadataService.SaveResult> results = service.createMetadata(new MetadataService.Metadata[] { customField });
             if(results.size()>0){
                if(results[0].success){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Object created successfully.');
                    ApexPages.addMessage(myMsg);
           
                }else{
                    if(results[0].errors.size()>0){
                        msg = results[0].errors[0].message;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,msg );
                        ApexPages.addMessage(myMsg);
                    }
                }
            }
        }    
    }

    public static MetadataService.MetadataPort createService()
    {
       MetadataService.MetadataPort service = new MetadataService.MetadataPort();
       service.SessionHeader = new MetadataService.SessionHeader_element();
       service.SessionHeader.sessionId = UserInfo.getSessionId();
        
        return service ;
    }
    
    public List<SelectOption> getAllObjects(){
        List<SelectOption> options = new List<SelectOption>();
        for ( Schema.SObjectType o : Schema.getGlobalDescribe().values() ){
            Schema.DescribeSObjectResult objResult = o.getDescribe();
            options.add(new SelectOption(objResult.getName(),objResult.getLabel()));
        }
        return options;
    }

}