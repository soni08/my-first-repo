public class AuraLightning{

     public static integer startPoint{get;set;}
     public static integer endPoint{get;set;}
     public static list<Contact> contList{get;set;}
     public static List<Contact> listToReturn{get;set;} 
     public static InnerModel Model;
 
    @AuraEnabled
    public static void insertContact(Contact obj){
    
        insert obj;
    }
    
    @AuraEnabled
    public static List<Contact> getContacts(){
      system.debug('%%%%%');
    
      contList = new List<Contact>();
      contList=[select id,firstname from contact];
        
      return contList;
      /*string jsn='[';
      for(contact cont: contList){
      
          jsn=jsn + '["'+cont.id+'"'+','+'"' +cont.firstname+'"' +',' +'"'+cont.lastname +'"'+',"'+cont.phone+'"],';
      } 
      //jsn= jsn.replace(jsn.substring(jsn.length()-1), '');
      jsn= jsn.substring(0, jsn.length()-1); 
      jsn=jsn+']';
      return jsn;
      */
    }
    
    @AuraEnabled
    public static List<Contact> nextRecord(){
    
      system.debug('&&&& called yes' +startpoint);
    //  startpoint =model.sp;
      //endpoint=model.ep;
      Integer i=0;
      listToReturn=new list<contact>();
      for(i=startpoint;i<=endpoint;i++){
      
        if(contList.size()>i)
            listToReturn.add(contList.get(i-1));

      }
      return listToReturn;
    
    }
    
    @AuraEnabled 
    public static job__c getJobDetails(String jobId){
        system.debug('--------->called');
        Job__c job = new Job__C();
        for(Job__c jobRecord: [select id,name,status__c,description__c,skills__c,expire_Date__c,no_of_posts__c,Domain__c, 
                                experience__c from job__c where id = : jobId]){
                            
                  job = jobRecord;
        }
        return job;
    }
    
    @AuraEnabled 
    public static String saveJobRecord(Job__C jobRecord){
       try{
           upsert jobRecord;
           return 'success';
       }catch(Exception exp){
           return exp.getMessage();
       }
    }
    @AuraEnabled
    public static String getListViews() {
        String viewId = '';
        List<ListView> listViews =  [SELECT Id, Name FROM ListView WHERE SobjectType = 'Job__C'];
        
        if(listviews.size()>0){
            viewId = listViews[0].id;
        }
        // Perform isAccessible() check here
        return viewId;
    }
    
    @AuraEnabled
    public static List<Account> searchAccount(String key){
        return [select id,name from account where name like : key + '%'];
    }
    
    @AuraEnabled
    public static List<Contact> getContacts(String accountId){
        return [select id,name,firstname,lastname,email from contact where accountid =: accountId];
    }
    
    public class  InnerModel{
    
        @AuraEnabled public integer sp{get;set;}
        @AuraEnabled public integer ep{get;set;}
    }
    

}