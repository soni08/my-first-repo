public class ApplicationForm{


    public Map<String,integer> stageMap{get;set;}
    public List<PieWedgeData> DataList{get;set;}
  
    private job__c job;
    
    public ApplicationForm(apexpages.StandardController std){
    
         
            
    
          DataList = new List<PieWedgeData>();
          job= (Job__C)std.getRecord();
          stageMap = new Map<String,integer>();
           Decimal totalApp;
          for(Application__c app : [select status__C,job__r.no_of_applicant__c from application__c where job__c=:job.Id ]){
                totalApp =app.job__r.No_of_Applicant__c;
              if(app.status__C !=null && stageMap.containsKey(app.status__c)){
              
                  Integer value = stageMap.get(app.status__C);
                  stageMap.put(app.status__c,++value);
              }
              else{
                  stageMap.put(app.status__c,1);              
              } 
          }
          if(stageMap !=null && stageMap.size()>0){
              for(String key : stageMap.KeySet()){

                  PieWedgeData obj = new PieWedgeData (key,(Decimal)stageMap.get(key) );
                  DataList.add(obj);
              }
          
          }      
    }
    // Wrapper class  
    public class PieWedgeData 
    {  
        public String name { get; set; }  
        public Decimal data { get; set; }  
        
        public PieWedgeData(String name, Decimal data) 
        {  
            this.name = name;  
            this.data = data;  
        }  
    }  
}