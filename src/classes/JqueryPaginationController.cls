Public class JqueryPaginationController{
    public List<Profile> profileList{get;set;}
    public List<Wrapper> wrapperList {get;set;}
    public JqueryPaginationController(){
        profileList = new List<Profile>();
        wrapperList  = new List<Wrapper>();
        profileList = [select id, name, lastmodifieddate from profile];
        for(Account acc : [select id,name from account]){
        
            Wrapper wrapperObj = new Wrapper();
            wrapperObj.name = acc.name;
            wrapperObj.id  = acc.id;
         
             wrapperList.add(wrapperObj); 
        }
    }

    public List<Account> getAccountList(){
    
        return [select id,name from account];
    }
    public string getAccList(){
        
        if(wrapperList !=null && wrapperList.size()>0)
            return JSON.serialize(wrapperList);
        else
            return '';    
    }

    public class Wrapper{
    
        public String id{get;set;}
        public string name{get;set;}
    }
}