public class GoogleMapDistance{
    public String result{get;set;}
    public String sourceAdd{get;set;}
    public String destinationAdd{get;set;}
    public List<RootObject> resultList{get;set;}
    
    public void  getJsonResults(){
        resultList = new List<RootObject>();
        String address1,address2;
        HttpRequest req = new HttpRequest();
        Http http = new Http();
    
        result='';
        address1=sourceAdd;
        address2=destinationAdd;
    
        req.setMethod('GET');
        String url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
            + '?origins=' + address1
            + '&destinations=' + address2
            + '&mode=driving'
            + '&sensor=false'
            + '&language=en'
            + '&units=imperial';
            
        req.setEndPoint(url);
        HTTPResponse resp;
        try{
             resp= http.send(req);     
             result='[';
             result +=resp.getBody();
             result+=']';
             resultList= (List<RootObject>)JSON.deserialize(result,List<RootObject>.class);
             String km='';
      
             if(resultList.size()>0){
             
                km = resultList[0].rows[0].elements[0].distance.text.substring(0,resultList[0].rows[0].elements[0].distance.text.length()-2);               
                km = (1.61 * double.valueOf(km))+'';
                
                resultList[0].origin_addresses[0]=resultList[0].origin_addresses[0].replace('(','');
                resultList[0].origin_addresses[0]=resultList[0].origin_addresses[0].replace(')','');
                string d=resultList[0].destination_addresses[0];
                 
                resultList[0].destination_addresses[0]=resultList[0].destination_addresses[0].replace('(','');
                resultList[0].destination_addresses[0]=resultList[0].destination_addresses[0].replace(')','');
                String s=resultList[0].destination_addresses[0];
                 
                result='<p style="color:green">Source Address :- '+s+'<br/>Destination Address :- '+d+'<Br/><br/>';
                result+='Distance :- '+km +' Km<br/> Duration :- '+resultList[0].rows[0].elements[0].duration.text+'</p>';
            }
         }
        catch(Exception ex){
           
            result='<p style="color:red">Invalid Address or Address Format..valid Format is (CityName,State/CountryName)</p>';
        }   
    }
    public class Distance
    {
        public string text { get; set; }
        public integer value { get; set; }
    }

    public class Duration
    {
        public string text { get; set; }
        public integer value { get; set; }
    }

    public class Element
    {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
        public string status { get; set; }
    }

    public class Row
    {
        public List<Element> elements { get; set; }
    }

    public class RootObject
    {
        public List<string> destination_addresses { get; set; }
        public List<string> origin_addresses { get; set; }
        public List<Row> rows { get; set; }
        public string status { get; set; }
    }
}