public class ShowJobsController{

      public map<string,string> jobMap{get;set;}
      
      public ShowJobsController(){
      
          jobMap = new map<string,string>();
          for(Job__c job : [select name from job__C])
              jobMap.put(job.name,job.id);      
      }

}