public class LogTimeSheetController{

    public List<Candidate> candidates{get;set;}
    public List<Dates> dates{get;set;}
    public LogTimeSheetController(){
        dates = new List<Dates>();
        candidates = new List<candidate>();
        DateTime dtObj ;
        for(Integer i=1;i<=Date.daysInMonth(system.today().year(),system.today().month());i++){
                 Dates dt = new Dates();
                 dtObj = DateTime.newInstance(system.today().year(),system.today().month(),i);
                 dt.day = dtObj.format('EEE')+'\n'+dtObj.day()+'/'+dtObj.month();
                 if(dtObj.format('EEE')=='Sun')
                     dt.Holiday = true;
                  
                 dates.add(dt);    
        }
        
        for(Contact cont: [select name,id,(select id,hours__C,day__C from log_hours__r where day__C=THIS_MONTH) from contact  where account.name='test']){
            
            candidate candi = new Candidate();
            candi.values = new List<String>();
            List<String> strList = new List<String>();
            strList.add(cont.name);
            //count hours for each day.
            boolean flag ;
            for(integer i=1;i<=Date.daysInMonth(system.today().year(),system.today().month());i++){
                flag = false;
                integer hrs = 0;
                for(Log_hours__C log : cont.log_hours__r){
                    integer d = log.day__C.day();
                    if(d==1){                      
                       flag = true;
                       hrs = integer.valueOf(log.hours__C);
                    }
                }
               if(flag)
                   strList.add(hrs+'');
               else    
                   strList.add('0');
            }
            candidates.add(Candi);
            
        }
            
    }
    
    public Class Dates{
    
        public string day{get;set;}
        public boolean holiday{get;set;}
    }
    
    public class Candidate{    
       public string name{get;set;}
       public integer hours{get;set;} 
       public List<string> values{get;set;}       
    }
}