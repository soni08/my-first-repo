@RestResource(urlMapping='/MyService/*')
global with sharing class MyRestResource {
    @Httppost
    global static String doPost() {
        // Insert Account Using Post Method
        Account  acc = new Account();
        String jsonStr = null;
    
        if (RestContext.request.requestBody  != null){
            jsonStr = RestContext.request.requestBody.toString();
            Map<String, object> m = (Map<String, object>)JSON.deserializeUntyped(jsonStr );
            system.debug('******'+m );
            acc.Name=(String)m.get('AccountName');
            insert acc;
        }
        return 'Account Inserted';
    }
    
   
    @Httpget
    global static List<Account> doget() {
        List<Account> accountList = new List<Account>();
        String jsonStr = null;
    
        RestRequest req = RestContext.request;
        String accountId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);           
        accountList  =  [SELECT Id, Name FROM Account where id =: accountId ];
        
        return accountList ;
        
    }
}