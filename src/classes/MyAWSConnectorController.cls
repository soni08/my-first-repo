public class MyAWSConnectorController{
    private String AWSCredentialName = 'NAME OF KEY TO USE';
    public S3.ListAllMyBucketsEntry[] allBucketList {get;set;}
    public S3.ListEntry[] bucketList {get;set;}
    public S3.AmazonS3 as3 { get; private set; } //This object represents an instance of the Amazon S3 toolkit and makes all the Web Service calls to AWS. 
    public String S3Key {get;set;}
    public String bucketNameToCreate {get;set;}
    public String OwnerId {get;set;}
    public Blob fileBlob {get;set;}
    public Integer fileSize {get;set;}
    public String bucketToUploadObject {get;set;}
    public String fileName {get;set;}
    public String accessTypeSelected {get;set;}
     
    public PageReference constructor(){
        try{
            
            List<AWSKey__c> keyObjList = [select key__c,secret__c,id from AWSKey__c limit 1]; 
            if(keyObjList.size()>0){
                as3 = new S3.AmazonS3(keyObjList[0].key__c,keyObjList[0].secret__c);
                S3Key = keyObjList[0].key__c;
            }   
        
        }catch(AWSKeys.AWSKeysException AWSEx){
             System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
             ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
             ApexPages.addMessage(errorMsg);     
        }

       return null; 
    }
    public PageReference createBucket(){
      try{   
           //SS createBucketErrorMsg= null;
           Datetime now = Datetime.now();        
           System.debug('about to create S3 bucket called: ' + bucketNameToCreate);      
           
           //This performs the Web Service call to Amazon S3 and create a new bucket.
           S3.CreateBucketResult createBucketReslt = as3.CreateBucket(bucketNameToCreate,null,as3.key,now,as3.signature('CreateBucket',now));
           System.debug('Successfully created a Bucket with Name: ' + createBucketReslt.BucketName);
          // SS createBucketErrorMsg='Success';
           return null;
       }
       catch(System.CalloutException callout){
          System.debug('CALLOUT EXCEPTION: ' + callout);
          ApexPages.addMessages(callout);
         //SS createBucketErrorMsg = callout.getMessage();
          return null;  
       }
       catch(Exception ex){
           System.debug(ex);
           ApexPages.addMessages(ex);
          //SS createBucketErrorMsg = ex.getMessage();
           return null; 
       }
    }
    //Method to return a string array for all the buckets in your AWS S3 account
    public String[] allBuckets { get {
        try{
        
        Datetime now = Datetime.now();
        
        //This performs the Web Service call to Amazon S3 and retrieves all the Buckets in your AWS Account. 
        S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(as3.key,now,as3.signature('ListAllMyBuckets',now));
        
        
        //Store the Canonical User Id for your account
        OwnerId = allBuckets.Owner.Id;
        
        S3.ListAllMyBucketsList bucketList = allBuckets.Buckets;
        S3.ListAllMyBucketsEntry[] buckets = bucketList.Bucket;
        allBucketList = buckets;
        
        String[] bucketNames = new String[]{};
        
        
        //Loop through each bucket entry to get the bucket name and store in string array. 
        for(S3.ListAllMyBucketsEntry bucket: buckets){
             System.debug('Found bucket with name: ' + bucket.Name);
             
             bucketNames.add(bucket.name);
            
        }
        
        return bucketNames;
        
        }catch (System.NullPointerException e) {
           return null;
        }catch(Exception ex){
           //System.debug(ex);
           System.debug('caught exception in listallmybuckets');
           ApexPages.addMessages(ex);
           return null; 
        }
        
        }//end getter
        set;
     }
     //This is used by the sample Visualforce page to display the list of all buckets created in your AWS account. 
     public List<SelectOption> getBucketNames() {
        List<SelectOption> options = new List<SelectOption>();        
        String[] bckts = allBuckets;
        if(bckts!=null){
            for(String bucket : allBuckets){
               options.add(new SelectOption(bucket,bucket));    
            }
            return options;
        }
        else
          return null;
     }
       public pageReference syncFilesystemDoc(){
        try{      
          Datetime now = Datetime.now();
          
          String docBody = EncodingUtil.base64Encode(fileBlob);
          
          //TODO - make sure doc.bodyLength is not greater than 100000 to avoid apex limits
          System.debug('body length: ' + fileSize);
          //ss uploadObjectErrorMsg = 'Error';
          Boolean putObjResult = as3.PutObjectInline_ACL(bucketToUploadObject,fileName,null,docBody,fileSize,accessTypeSelected,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, OwnerId);
          if(putObjResult==true){
              System.debug('putobjectinline successful');
              //ss uploadObjectErrorMsg = 'Success';
          }
          
      
        }catch(System.CalloutException callout){
          System.debug('CALLOUT EXCEPTION: ' + callout);
          //ss uploadObjectErrorMsg =    callout.getMessage();
    
        }catch(Exception ex){
            System.debug('EXCEPTION: ' + ex);
           //ss uploadObjectErrorMsg =  ex.getMessage();
        }
       
      return null;  
    }
    public void uploadToAmazonS3 (Attachment attach, String folderName) {
         
        String filename = folderName+'/' + attach.Name;
        String attachmentBody = EncodingUtil.base64Encode(attach.Body);
        String formattedDateString = DateTime.now().formatGMT('EEE, dd MMM yyyy HH:mm:ss z');
        String bucketname = 'test'; //you can write the bucket name where files should be uploaded
        String host = 'test';  //aws server base url
         
        HttpRequest req = new HttpRequest();
        req.setMethod('PUT');
        req.setEndpoint('https://' + bucketname + '.' + host + '/' + filename);
        req.setHeader('Host', bucketname + '.' + host);
        req.setHeader('Content-Length', String.valueOf(attachmentBody.length()));
        req.setHeader('Content-Type', attach.ContentType);
        req.setHeader('Connection', 'keep-alive');
        req.setHeader('Date', formattedDateString);
        req.setHeader('ACL', 'public-read-write');
        Blob blobBody = EncodingUtil.base64Decode(attachmentBody);
        req.setBodyAsBlob(blobBody);
   }
}