public class Lightning_ExampleController{

    private String accId;
    public Account accObj{get;set;}
    public Lightning_ExampleController(){
        accObj = new Account();
        accId = apexpages.currentpage().getParameters().get('id');
        if(String.isNotBlank(accId)){
            for(Account acc : [select id,name,website,phone,ParentId from account where id =:accId ]){
                accObj =  acc;
            }
        }
    }
    
    public List<Contact> getContactList(){
   
        return [select id,name,email,phone from contact where accountId =: accId];
  
    }
    
    @remoteaction
    public static List<AccountModel> fetchAccounts (String key){
        List<AccountModel> accounts = new List<AccountModel>();
        for(Account acc : [select id,name from account where name like : key +'%']){
            accounts.add(new AccountModel(acc.id,acc.name));
        }
        return accounts ;
    }

    public class AccountModel{
        public String id;
        public String label;
        
        public AccountModel(String accId,String accLabel){
            id = accId;
            label = accLabel;
        }
    }
}