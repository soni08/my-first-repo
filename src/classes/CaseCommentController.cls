public class CaseCommentController{

    public List<InnerModel> files{get;set;}
    
    public CaseCommentController(){
    
        files = new List<InnerModel>();
        String caseId =  apexpages.currentpage().getParameters().get('id');
    
        for(comment__c cmt : [select id,name,comment_body__c,(select id,contenttype from attachments) from comment__c where case__C =: caseId]){
            
            string body = cmt.comment_Body__c;
            InnerModel model = new InnerModel();
            model.attachments = new List<Attachment>();
            model.imageUrls = new List<String>();
            
            //Attachments
            for(Attachment attachObj : cmt.attachments)
               model.attachments.add(attachObj);
            
            model.comment = cmt;
            if(body != null && body.contains('<img ')){
                integer i;
                string tempBody = body;
               for(i=0;i<tempBody.countMatches('<img ');i++){

                    String src = body.substring(body.indexOf('src="http')+5);   
                    if(src !=null && src.substring(0,src.indexOf('"')) !=null){           
                        model.imageUrls.add(src.substring(0,src.indexOf('"')).stripHtmlTags());
                        body =  body.substring(body.indexOf('</img>')+5);
                        system.debug('---->'+i+'--->'+body);
                    }   
               }         
           }
          files.add(model);
        }
    }
    
    public class InnerModel{
        
        public Comment__c comment{get;set;}
        public List<attachment> attachments{get;set;}  
        public List<string> imageUrls{get;set;}
        public integer count{get;set;}
    }

}