global with sharing class SavePdf{



    webservice static void save(string vrdId){
    
       pagereference pg = new pagereference ('/apex/RenderPdf');
       Blob b = pg.getContentAsPdf();
       Account acc = new Account();
       acc = [select id,name from account where id=:vrdId];
       Attachment attach = new Attachment();
       attach.name=acc.name+'_pdf';
       attach.contentType = 'application/pdf';
       attach.body = b;
       attach.parentId = vrdId;
       insert attach;
    
    }



}