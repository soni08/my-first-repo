public class sendEmailWithAttachment{

    public blob attach{get;set;}
    public string subject{get;set;}
    public string body{get;set;}
    public string emailId{get;set;}
    public sendEmailWithAttachment(ApexPages.StandardController controller) {
      
      
    }

    public pagereference sendEmail(){
       
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
      
        mail.setToAddresses(new String[] { emailId });
        
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
     
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();  
            efa.setFileName('Testing');
            efa.setBody(attach);
            fileAttachments.add(efa);
        
        mail.setPlaintextbody(body);
        mail.setSubject(subject);

        mail.setFileAttachments(fileAttachments);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
        Task tsk = new Task();
        tsk.whoid='00328000006hirn';
        tsk.subject=subject;
        insert tsk;
        
        Attachment att = new Attachment();
        att.body=attach;
        att.parentid=tsk.id;
        att.name=subject;
        insert att;

        return new pagereference('/'+tsk.id);
    }
}