global class SendMMSToLeadBatchController implements Database.Batchable<sObject>,Database.AllowsCallouts{

        global SendMMSToLeadBatchController (){
                   // Batch Constructor
        }
       
        // Start Method
        global Database.QueryLocator start(Database.BatchableContext BC){
          String query = 'SELECT mobilephone FROM lead where mobilephone !=null  and (name=\'Sagar Soni\' or name =\'Nitin Chandwani\') limit 2';
        
         return Database.getQueryLocator(query);
        }
      
      // Execute Logic
       global void execute(Database.BatchableContext BC, List<lead>scope){
            for(lead ld : scope){
                   try{
                       TwilioIntegrationForSMS.sendTextMessage(ld.mobilePhone,'Hello','http://www.freedigitalphotos.net/images/img/homepage/87357.jpg');   
                   }catch(Exception exp){}
                   
             } 
       }
     
       global void finish(Database.BatchableContext BC){
            // Logic to be Executed at finish
       }
    }