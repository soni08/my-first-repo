public class GoogleDriveController  
    {  
        //Fetched from URL  
        private String code ;  
        private string key = '726671845416-h8gf8gnj1m3ervar50ralp2bt5hc90mk.apps.googleusercontent.com' ;  
        private string secret = 'Vnnq81oncFE2uPr_TIwzRi9Q' ;  
        private string redirect_uri = 'https://c.ap2.visual.force.com/apex/ConnectGoogleDrive' ;  
        private List<InnerModel> modelList;  
        public list<GoogleDriveJsonController> files{get;set;} 
        public string downLoadUrl{get;set;}
        public string type{get;set;}
        public string fileId{get;set;}
        public string fileName{get;set;}
        public GoogleDriveController()  
        {  
            files = new list<GoogleDriveJsonController > ();
            modelList = new List<InnerModel>();
            code = ApexPages.currentPage().getParameters().get('code') ;  
          // Get the access token once we have code  
            if(code != null && code != '')  
            {  
                AccessToken() ;  
            }  
            //DriveAuth();
        }  
          
        public PageReference DriveAuth()  
        {  
            //Authenticating  
            PageReference pg = new PageReference(GoogleDriveAuthUri (key , redirect_uri)) ;  
            return pg ;  
        }  
          
        public String GoogleDriveAuthUri(String Clientkey,String redirect_uri)  
        {  
            String key = EncodingUtil.urlEncode(Clientkey,'UTF-8');  
            String uri = EncodingUtil.urlEncode(redirect_uri,'UTF-8');  
            String authuri = '';  
            authuri = 'https://accounts.google.com/o/oauth2/auth?'+  
            'client_id='+key+  
            '&response_type=code'+  
            '&scope=https://www.googleapis.com/auth/drive'+  
            '&redirect_uri='+uri+  
            '&state=security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome&'+ 
            '&login_hint=sagar.soni@ibirdsservices.com&'+  
            'access_type=offline';  //we can specify offline here then it will work for fix gmail account not for all. 
            return authuri;  
        }  
          
          
        public void AccessToken()  
        {  
            //Getting access token from google  
            HttpRequest req = new HttpRequest();  
            req.setMethod('POST');  
            req.setEndpoint('https://accounts.google.com/o/oauth2/token');  
            req.setHeader('content-type', 'application/x-www-form-urlencoded');  
            String messageBody = 'code='+code+'&client_id='+key+'&client_secret='+secret+'&redirect_uri='+redirect_uri+'&grant_type=authorization_code';  
            req.setHeader('Content-length', String.valueOf(messageBody.length()));  
            req.setBody(messageBody);  
            req.setTimeout(60*1000);  
      
            Http h = new Http();  
            String resp;  
            HttpResponse res = h.send(req);  
            resp = res.getBody();  
              
            System.debug(' You can parse the response to get the access token ::: ' + resp);  
            string resBody='[';
      
            if(resp!=null){
               resBody +=   resp;
               resBody += ']';
               resp=resBody ;
               modelList = (List<InnerModel>)JSON.deserialize(resp,List<InnerModel>.class);
            
             }
             System.debug('#####' + modelList[0].access_token);      
       }  
       
       public void createFile(){
        
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint('https://www.googleapis.com/upload/drive/v2/files?uploadType=media');
            req.setHeader('content-type', 'image/png');
            req.setHeader('Authorization','Bearer '+modelList[0].access_token);
           // String messageBody = 'Hi, This message is from Salesforce (INTEGRATION WORK) FROM SAGAR SONI';
             blob attach =[select body from attachment where id='00P280000013QvN' limit 1].body;
         
            //req.setMessageBody(messageBody);
            req.setBodyAsBlob(attach); 
            req.setTimeout(60*1000);
            HttpResponse resp = http.send(req);
            if(resp!=null)
                system.debug('*****##' +resp.getBody());
       }
       
       public void downloadFile(){
      
            files = new list<GoogleDriveJsonController >();
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
           // req.setEndpoint('https://doc-14-bs-docs.googleusercontent.com/docs/securesc/p6rfftpms69l889fjlj1fatsnr7ampit/hfeoirv8pf6r9l94f139usdba5b3mamj/1443780000000/10009199110860855191/10009199110860855191/0BzKPXr_wcxTiSUFnOTg4anhLS0E?h=03180420926048321916&e=download&gd=true');
            req.setEndpoint('https://www.googleapis.com/drive/v2/files');
            //req.setEndpoint('https://googledrive.com/host/me');
           // req.setHeader('content-type', 'image/png');
            req.setHeader('Authorization','Bearer '+modelList[0].access_token);
            req.setTimeout(60*1000);
            HttpResponse resp = http.send(req);
            system.debug('@@@@@' + resp +'&&'+resp.getBody());
            if(resp.getStatusCode()==200){
                string body=resp.getBody();
                if(body.contains('/'))
                    body=body.replace('/','_2F');
                if(body.contains('.'))    
                    body=body.replace('.','_2E');
               if(body.contains('-'))
                   body=body.replace('-','_2D');
            String temp='[' + body;
            system.debug('//////////' +  body);
            body =temp +']';
            files = (List<GoogleDriveJsonController >)JSON.deserialize(body,List<GoogleDriveJsonController >.class);
            system.debug('%%%%%' + files.size());
           /* attachment attach = new attachment();
            attach.body=resp.getBodyAsBlob();
         
            attach.name='download from google';
            attach.parentid='00128000008HhgN';
            attach.contenttype='image/png';
            insert attach;*/
            }
           
       }
       public pagereference showFile(){
       
          
          system.debug('^^^^^55called' +downLoadUrl +'***'+type ); 
          
          if(!String.isBlank(fileId)){ 
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
              //  req.setEndpoint('https://googledrive.com/host/'+''+fileId); 
              req.setEndpoint(downLoadUrl);
               req.setHeader('content-type', type);
                req.setHeader('Authorization','Bearer '+modelList[0].access_token);
                req.setTimeout(60*1000);
                HttpResponse resp = http.send(req);
                system.debug('@@@@@' + resp +'&&'+resp.getBodyAsBlob());
                if(resp.getStatusCode()==200){
    
                   // Blob b= blob.valueOf(resp.getBody()); 
                    attachment attach = new attachment();
                    attach.body=resp.getBodyAsBlob();
                    attach.contenttype = type;
                    attach.name= fileName;
                    attach.parentid='00328000006hh0w';
                    
                    insert attach;
                    
                   
                   return new pagereference('/'+attach.id);
              
                }
                else{
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,resp.getStatus());
                        ApexPages.addMessage(myMsg);
                        return null;
               
                    return null;
                    
                }    
           }else
               return null;
       }
       class InnerModel{
       
           public string access_token;
           public string token_type;
          //_2F for'/'
          //_2D for '-'
          //_2E For dot
           public string expires_in;
       
       }
       
    }