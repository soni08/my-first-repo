global class EmailDemoReceive implements Messaging.InboundEmailHandler {

   global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                                                         Messaging.Inboundenvelope envelope) {
   Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        system.debug('called--->');
        String subToCompare = 'Create Contact';
        List<Attachment> attachmentList = new List<Attachment>();
       // if(email.subject.equalsIgnoreCase(subToCompare)){
            String fromAddress = email.fromAddress;
            system.debug('email --->' + fromAddress );
            List<Contact> contactList = [select id from contact where email =:fromAddress limit 1];
            if(contactList !=null && contactList.size()>0){
                 system.debug('entereed-->' );
                // Save attachments, if any
                if(email.textAttachments !=null && email.textAttachments.size()>0){
                    for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
                        Attachment attachment = new Attachment();
            
                        attachment.Name = tAttachment.fileName;
                        attachment.Body = Blob.valueOf(tAttachment.body);
                        attachment.ParentId = contactList[0].Id;
                        attachmentList.add(attachment);
                       //insert attachment;
                    }
                }
                //Save any Binary Attachment
                if( email.binaryAttachments !=null &&  email.binaryAttachments.size()>0){
                    for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                        Attachment attachment = new Attachment();
            
                        attachment.Name = bAttachment.fileName;
                        attachment.Body = bAttachment.body;
                        attachment.ParentId = contactList[0].Id;
                       attachmentList.add(attachment);
                        //insert attachment;
                    }
                }    
                if(attachmentList.size()>0){
                    try{
                        system.debug('inserted-->' );
                        insert attachmentList;
                        result.success = true;  
                    }Catch(Exception exp){
                        result.success = false; 
                        system.debug('exception-->' + exp.getMessage() ); 
                    }
                }
                
            }     
       // }
        return result;
    }

}