global class SendEmailController{

    @AuraEnabled
    public static string fetchContent(String contactId){
        String contName ,email ;
        for(Contact cont : [select id,email,name from contact where id=:contactId and email!=null]){
            contName  = cont.name;
            email  = cont.email;
        }
        
        if(!String.isBlank(email)){
            String emailBody = 'Hi ' + contName +', \n \n' + 'Please upload your electronic copies of documents in the following formats : \n \n';
            emailBody += '.doc; ,docx; .rtf; .pdf; .txt; .jpg; .xls; .xlsx; .gif. \n\nYou can upload multiple files and each document must be no larger than 20MB.\n\n';
            
      
    
            emailBody += 'Upload now by click the link below or reply this email with doucments attached.\n\n';
            
          
            emailBody += 'Keep documents small to improve processing time.\n\n';
            emailBody += 'Thank you';
         
            return emailBody;
        }else{
            return 'Email not found.';
        }    
    }
    @AuraEnabled
    public static List<ContactWrapper> fetchContacts(String recordId){
        List<ContactWrapper> contactList = new List<ContactWrapper> ();

        if(!String.isBlank(recordId) && recordId.startsWith('001')){
          
            for(Contact cont : [select id,email,name from contact where accountid =:recordId]){
                contactList.add(new ContactWrapper(cont.name,cont.email,cont.id));
            }
        }else if(!String.isBlank(recordId) && recordId.startsWith('006')){
           for(opportunitycontactrole ocr : [SELECT Id, contact.name,contactId, contact.email FROM OpportunityContactRole where OpportunityId =: recordId and contactId !=null]){
                
                contactList.add(new ContactWrapper(ocr.contact.name,ocr.contact.email,ocr.contactId));
            }
        }
        
        return contactList;
    }
     @AuraEnabled
    public static string getData(String postcode,string street){
        
        HttpRequest rqst=new HttpRequest();
        string endpoint1 = 'https://addressvalidate.herokuapp.com/?stname='+street+'&pcode='+postcode ;
        rqst.setEndPoint(endpoint1);
        rqst.setMethod('GET');
          rqst.setHeader('Authorization', 'OAuth ' + userinfo.getSessionId());
        rqst.setTimeout(120000);
        
        Http obj=new Http();
        HttpResponse res= obj.send(rqst);
        system.debug('result>>>'+res.getBody());
        return res.getBody();
  
    }
    @AuraEnabled
    public static string getCity(String cityName){
        
        HttpRequest rqst=new HttpRequest();
        string endpoint1 = 'https://addressvalidate.herokuapp.com/fetchCities.php?cityName='+cityName ;
        rqst.setEndPoint(endpoint1);
        rqst.setMethod('GET');
          rqst.setHeader('Authorization', 'OAuth ' + userinfo.getSessionId());
        rqst.setTimeout(120000);
        
        Http obj=new Http();
        HttpResponse res= obj.send(rqst);
        system.debug('result>>>'+res.getBody());
        return res.getBody();
  
    }
    @AuraEnabled
    public static string sendEmailToContact(String recordId,string emailBody){
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        email.setTargetObjectId(recordId);
        email.setSubject('Upload Document Request');   
        email.setPlainTextBody(emailBody);
        email.setSaveAsActivity(true);
        try{
        
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
            return 'Email sent successfully.';
        }catch(exception exp){
            return exp.getMessage();
        }  
        
  
    }
   public class ContactWrapper{
   @AuraEnabled
       public string name{get;set;}
       @AuraEnabled
       public string email{get;set;}
       @AuraEnabled
       public boolean status{get;set;}
       @AuraEnabled
       public string id{get;set;}
       public ContactWrapper(string name,string email,string id){
           this.name = name;
           this.email = email;
           this.id = id;
       }
   }
}