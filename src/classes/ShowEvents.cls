public class ShowEvents{

    public list<Event> eventList{get;set;}
    public string eventDate{get;set;}
    public string eventId{get;set;}
    public string title{get;set;}
    public string endDate{get;set;}
    public string startDate{get;set;}
    
    public ShowEvents(){
    
        eventList = new List<Event>();
        String id =apexpages.currentpage().getParameters().get('id');
        id='00328000006hirn';
        if(id!=null){
            eventList =[select id,subject,activitydate,startdatetime,enddatetime from event where whoid =:id];
        }
    }
    
   public void insertEvent(){
   
        Event ev = new Event();
      
        ev.subject = title;
        String[] arr= startDate.split('/');
        string m = arr[0];
        string d =  arr[1];
        string y = arr[2];

        ev.startdatetime= Date.newInstance(Integer.valueOf(y),Integer.valueOf(m),Integer.valueOf(d)+1);
            
        arr = endDate.split('/');
        m = arr[0];
        d =  arr[1];
        y = arr[2];
        
        ev.enddatetime= Date.newInstance(Integer.valueOf(y),Integer.valueOf(m),Integer.valueOf(d)+1);
        ev.whoid ='00328000006hirn';
        try{
            insert ev;
            eventList =[select id,subject,activitydate from event where whoid ='00328000006hirn'];
        }
        catch(Exception e){
        
        }
     
   }
   public void updateDate(){
    
        Event ev = new Event(id=eventId);
        String[] arr= eventDate.split('/');
        string m = arr[0];
        string d =  arr[1];
        string y = arr[2];
        system.debug('@@@@' + d);
        ev.startdatetime= Date.newInstance(Integer.valueOf(y),Integer.valueOf(m),Integer.valueOf(d)+1);
        try{
             update ev;
        }
        catch(Exception e){}
        System.debug('*******' + ev.activitydate +  '&&&'+ ev.id);
        eventList =[select id,subject,activitydate from event where whoid ='00328000006hirn'];
        
    
    }

}