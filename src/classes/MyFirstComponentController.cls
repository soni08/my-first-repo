global class MyFirstComponentController{

    @auraEnabled
    public static List<Contact> getContactList(){
    
        return [select id,name,email,phone from contact order by createddate desc];
    }
    
    @auraEnabled
    public static Id createContact(Contact cont){
        
    
        upsert cont;
        return cont.id;
    
    }
    
    @auraEnabled
    public static List<Account> fetchAccounts(string key){
        
        return [select id,name from account where name like : key +'%'];
    
    }
     @auraEnabled
    public static List<Account> fetchAllAccounts(){
        
        return [select id,name from account ];
    
    }
    
    @auraEnabled
    public static List<Contact> getContactRecord(String key){
        
        return [select id,firstname,lastname,name,email,phone from contact where id =: key];
    
    }




}