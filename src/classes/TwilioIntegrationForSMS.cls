public with sharing class TwilioIntegrationForSMS {
    //Required parameters
    private static String VERSION ;
    private static String endpoint;
    public  static String DEFAULT_VERSION;
    // Variable to hold the reponse
   
    public MessageResponse smsRes;
    
    //Method to hit http request
    public static string sendTextMessage(string mobileNumber,string textMessage,String Media) {
        integer statusCode =0;
        string accountSid;
        string authToken;
        string phoneNumber;
        MessageResponse smsRes = new MessageResponse();
        
        //Referencing custom setting data       
       // List<TwilioSetting__c> twSet=[select VERSION__c,endpoint__c,DEFAULT_VERSION__c,name,AccountSid__c,AuthToken__c,twilio_number__C from TwilioSetting__c where name='Twilio Account' limit 1];
       // if(twSet.size() > 0){
            accountSid = 'AC878ce0a2e99421e7dd131358e083497f';
            authToken = '4f9e4d159401931b1fbc63c13ebf09cd';
            phoneNumber = '+12019425690' ;  
            DEFAULT_VERSION = '2010-04-01';
            endpoint = 'https://api.twilio.com';
            VERSION = '3.2.0';
       // }
        //variable ot hold the response
        String responseBody = '';
        
        //Endpoint URL
        String path = endpoint + '/' + DEFAULT_VERSION + '/Accounts/' + accountSid + '/Messages.json';
        URL uri = new URL(path);
        String entity = '';
        
        //Check for mobile number and text message

        if(mobileNumber != Null) {
            entity += (entity=='' ? '' : '&')+ 'To' + '=' + EncodingUtil.urlEncode(string.valueOf(mobileNumber), 'UTF-8');
            entity += (entity=='' ? '' : '&')+ 'From' + '=' + EncodingUtil.urlEncode(phoneNumber, 'UTF-8');
            entity += (entity=='' ? '' : '&')+ 'Body' + '=' + EncodingUtil.urlEncode(textMessage, 'UTF-8');
            if(Media!=null && Media!='')
                entity += (entity=='' ? '' : '&')+ 'MediaUrl' + '=' + EncodingUtil.urlEncode(Media, 'UTF-8');
        } 
                
        //Initiallize http request
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        
        //Append parameters to the request
        request.setHeader('X-Twilio-Client', 'salesforce-' + VERSION);
        request.setHeader('User-Agent', 'twilio-salesforce/' + VERSION);
        request.setHeader('Accept', 'application/json');
        request.setHeader('Accept-Charset', 'utf-8');
        request.setHeader('Authorization', 'Basic '+EncodingUtil.base64Encode(Blob.valueOf(accountSid + ':' + authToken)));
        request.setEndpoint(uri.toExternalForm());
        request.setMethod('POST');
        request.setBody(entity);
    
        //Check for text is running 
        if(!Test.isRunningTest()) { 
            try{            
                //Send request
                HttpResponse res = h.send(request);
                responseBody = res.getBody();
                statusCode = res.getStatusCode();
                System.debug('responseBody:::::::::::' +responseBody);  
            }catch(Exception ex) {   
                 system.debug('***Exception***: ' + ex);
            }
        } else {
            responseBody =  '{"sid":"SMba4ab2fafa3d709004359818f50a67eb","date_created":"Thu, 12 Dec 2013 15:13:06 +0000","date_updated":"Thu, 12 Dec 2013 15:13:06 +0000","date_sent":null,"account_sid":"ACfdd0fcec687020b86d09fa8451c4b614","to":"+919820242561","from":"+13047132065","body":"Hello there!","status":"queued","direction":"outbound-api","api_version":"2010-04-01","price":-0.01,"price_unit":"USD","uri":"/2010-04-01/Accounts/ACfdd0fcec687020b86d09fa8451c4b614/SMS/Messages/SMba4ab2fafa3d709004359818f50a67eb.json","num_segments":"1"}';
            
        }
        smsRes = (MessageResponse)JSON.deserialize(responseBody, MessageResponse.class);
        System.debug('smsRes:::::::' + smsRes);
        
        String invalidNumber = smsRes.message;
        System.debug('invalidNumber:::::::' + invalidNumber);
        if(statusCode==201)
            return responseBody;
        else    
            return 'Error';
    }
    //Wrapper class fro response
    public class MessageResponse {
        public String status;
        public String message;
    }     
}