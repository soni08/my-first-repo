@RestResource(urlMapping='/MyRest/*')
global with sharing class MyRestController{

      @HttpPost  
      global static void createNewCase() {
      
          RestContext.response.addHeader('Content-Type', 'application/json'); 
          List<Contact> contList = new List<Contact>();
          contList=[select id,firstname,lastname from contact where firstname!=null];
          String jsonString='[';
          for(Contact cont:contList){
              jsonString +='{"firstname":"'+cont.firstname+'","lastname":"'+cont.lastname+'"},';
          }
          jsonString=jsonString.substring(0,jsonString.length()-1);
          jsonString += ']';
          
          RestContext.response.responseBody = Blob.valueOf(jsonString);
          String requestBody;
          requestBody = RestContext.request.requestBody.toString();
          system.debug('####'+requestBody);       
          //return 'hellllooo guys';
      } 
    @HttpGet
    global static void sendEmail(){
    
        RestRequest req = RestContext.request;
        String contId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        String tId = [select id from emailtemplate where developername='WebServiceTemplate'].id;
        Contact cont = [select id,email from contact where id=:contId];
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(tId);         
        email.setTargetObjectId(contId);
        email.setSaveAsActivity(false);
        if(cont!=null && cont.email!=null)
            email.setToAddresses(new List<String>{cont.email});
        //email.setSubject('Web Service Practising');
       // email.setPlainTextBody('Hello , This is sagar soni with Salesforce');
        
        if(cont!=null && cont.email!=null)
             Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
    }  
}