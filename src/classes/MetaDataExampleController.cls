public class MetaDataExampleController{

   public list<Modal> modalList{get;set;}
   private integer i{get;set;}
   public List<selectOption> operators{get;set;}
   public List<selectOption> options{get;set;} 
   public string indx{get;set;}
    
    
    public MetaDataExampleController(){
    
        i=0;
        modalList = new list<Modal>();
        options = new List<selectOption>();
        operators = new List<selectOption>();
      
        createRow();
        addRow();  
    }
    public void addRow(){
    
         if(options!=null && options.size()>0){
         
              Modal modalObj  = new Modal();
              modalObj.opList = operators ;
              modalObj.fields = options;
              modalObj.index = ++i;
              modalList.add(modalObj);
         }
            
    }
    public void delRow(){
    
        integer j=0;
        for(Modal obj : modalList){
            
             if(String.valueOf(obj.index) == indx){
             
                 modalList.remove(j);
                 break;
             }
             j = j + 1;
        
        } 
        
        i = 0 ;
        for(Modal obj : modalList){
        
            obj.index = ++i;
        
        }    
    }
    
    public void createRow(){
    
        operators.add(new selectoption('equal','equal'));
        operators.add(new selectoption('not equal','not equal'));
        operators.add(new selectoption('greater than','greater than'));
        operators.add(new selectoption('less than','less than'));
        
        List<String> lookupfields = new List<String>();
        set<Schema.sObjectType> lkfields = new set<Schema.sObjectType>();

        SObjectType acc = Schema.getGlobalDescribe().get('Opportunity');
        Map<String,Schema.SObjectField> allFields = acc.getDescribe().fields.getMap();
        for(String key  : allFields.keyset()){
            Schema.DescribeFieldResult field = allFields.get(key).getDescribe();
            List<Schema.sObjectType> lstRef = new List<Schema.sObjectType>();
            lstRef = field.getreferenceTo();
            if(lstRef == null ||  lstRef.size()==0)
                options.add(new selectoption('Opportunity : '+field.getName(),'Opportunity : ' +field.getLabel()));
            else{
              // lookupfields.add(field.getName()); 
                // lookupfields.add(field.getrelationshipName());
                lkfields.add(lstRef[0]); 
             }    
        }        
        //-------------------
        
         system.debug('****' +lkfields);
        if(lkfields != null && lkfields.size()>0){
            for(sObjectType type : lkfields){
               
                Map<String,Schema.SObjectField> allFields1 = type.getDescribe().fields.getMap();
                for(String key  : allFields1.keyset()){
                    if(allFields1.containsKey(key)){
                        Schema.DescribeFieldResult field = allFields1.get(key).getDescribe();
                        options.add(new selectoption('Opportunity : '+type+' : '+ field.getName(),'Opportunity : '+type+'  : ' +field.getLabel()));
                     }
                }
               
            }
        }

    }
    
    public class Modal{
        
        public integer index{get;set;}
        public String selectedField{get;set;}
        public String selectedOperator{get;set;}
        public String value{get;set;}
        public List<selectOption> fields{get;set;}
        public List<selectOption> opList{get;set;}
 
    }
}