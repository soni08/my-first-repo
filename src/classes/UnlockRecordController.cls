public class UnlockRecordController{

    public unlockRecordController(apexpages.standardController std){}
    
    
    

    public string result{get;set;}
    public void unlock(){

    // Query the accounts to lock
        application__c[] accts = [SELECT Id from application__C WHERE id= 'a0328000005vpUI'];
        // Lock the accounts
        Approval.unLockResult[] lrList = Approval.unlock(accts, false);
        
        // Iterate through each returned result
        for(Approval.unLockResult lr : lrList) {
            if (lr.isSuccess()) {
            result =  lr.getId();
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully locked account with ID: ' + lr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : lr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
    
    
    
    public void request(){
    
        // create the new approval request to submit
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitted for approval. Please approve.');
        req.setObjectId('a0328000005vpUI');
        // submit the approval request for processing
        Approval.ProcessResult result = Approval.process(req);
        // display if the reqeust was successful
        System.debug('Submitted for approval successfully: '+result.isSuccess());
    }
    
    
}