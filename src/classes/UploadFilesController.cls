public class UploadFilesController{

    private Attachment attach;
    public string blobValue{get;set;}
    public string type{get;set;}
    public string name{get;set;}
    public string message{get;set;}
    public string contactName{get;set;}
    private string contId;
    
    public UploadFilesController(){
        attach = new Attachment();      
        contId = apexpages.currentpage().getParameters().get('id');
        for(Contact cont : [select id,name from contact where id =: contId limit 1]){
            contactName = cont.name;
        }
    }
    
    
    public void insertAttachment(){
      
        if(!String.isBlank(contId) && !String.isBlank(contactName)){
            if(!String.isBlank(blobValue)){
                attach.parentId = contId ;
                attach.body = EncodingUtil.base64Decode(blobValue);
                attach.name = name;
                attach.contentType = type;
                try{
                    insert attach;
                    system.debug('inserted----->');
                    message = 'File uploaded successfully.';
                }Catch(Exception exp){
                    message = exp.getMessage();
                } 
                   
                attach = new Attachment();
                blobValue = '';
             }else{
                 message = 'Please attach a file.';
                 blobValue ='';
                 attach = new Attachment();
             } 
                
       }else{
           message = 'No contact found.';
           blobValue ='';
           attach = new Attachment();
       }     
    }
   
}