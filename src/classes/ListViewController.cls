public class ListViewController{

    public string jsn{get;set;}
    public List<fromJSON> listView {get;set;}
    public string query;
    public List<account> accList{get;set;}
    public List<String> fields{get;set;}
   
    public ListViewController(){
   
       fields = new List<String>();
       listView  = new List<fromJSON>();
       accList = new List<account>();
       //HTTP callouts
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        req.setHeader('Content-Type', 'application/json');
        String domainUrl=URL.getSalesforceBaseUrl().toExternalForm();
        String endpointUrl=domainUrl+'/services/data/v32.0/sobjects/Account/listviews/00B28000003S9ML/describe';
        req.setEndpoint(endpointUrl);
        req.setMethod('GET');      
        Http h = new Http();
        HttpResponse res = h.send(req);
        system.debug('##jason' + res.getBody());
        jsn = res.getBody();
        if(jsn !=null){
        string temp = '[';
        temp += jsn+']';
        jsn = temp;
        listView = (List<fromJSON>)JSON.deserialize(jsn,List<fromJSON>.class);
        if(listView.size()>0){
           query = listView[0].query;
           accList = Database.query(query);
           for(cls_columns field : listView[0].columns){
               if(field.fieldNameOrPath=='Id')
                   break;   
               fields.add(field.fieldNameOrPath);
           }
        }
          
       /* Map<String,Object> root  = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());     
         
        //this logic to over come the 10k collection limit    
        for(Sobject sobj : Database.query((string)root.get('query'))){
          recordList.add(sobj);
          if(recordList.size() == 10000){
             allRecords.add(recordList);
             recordList = new List<sobject>();
          }
        }
             
        if(recordList != null && !recordList.isEmpty())
          allRecords.add(recordList);  
        
        //Parsing to get the column details        
        JSONParser parser = JSON.createParser(res.getBody());
        while (parser.nextToken() != null){ 
           if(parser.getCurrentToken() == JSONToken.START_ARRAY) {       
             while (parser.nextToken() != null) {
                if(parser.getCurrentToken() == JSONToken.START_OBJECT) {
                   listviewAPI.Columns le = (listviewAPI.Columns)parser.readValueAs(listviewAPI.Columns.class);
                   //parserCol.add(le);   
                   system.debug('###'+le);     
                 }
             }
           }
        }
        */     
        } 
     }   
     public class fromJSON{
        public cls_columns[] columns;
        public String id;   //00B28000003S9ML
        public cls_orderBy[] orderBy;
        public String query{get;set;}    //SELECT name, phone, billingstate, id, recordtypeid, createddate, lastmodifieddate, systemmodstamp FROM Account WHERE BillingState != null ORDER BY Name ASC NULLS FIRST, Id ASC NULLS FIRST
        public cls_scope scope;
        public String sobjectType;  //Account
        public cls_whereCondition whereCondition;
    }
 
    class cls_scope {
    }
    class cls_whereCondition {
        public String field;    //BillingState
        public String operator; //notEquals
        //public cls_values[] values;
    }
 
    class cls_columns {
        
        public String ascendingLabel;   //Z-A
        public String descendingLabel;  //A-Z
        public String fieldNameOrPath;  //Name
        public boolean hidden;
        public String label;    //Account Name
        public String selectListItem;   //Name
        public String sortDirection;    //ascending
        public Integer sortIndex;   //0
        public boolean sortable;
        public String type; //string
        
    }
    class cls_values {
    
     /* public String val1;    //n
        public String val2;    //u
        public String val3;    //l
        public String val4;    //l
*/
    }  
    class cls_orderBy {
        public String fieldNameOrPath;  //Name
        public String nullsPosition;    //first
        public String sortDirection;    //ascending
    }

}